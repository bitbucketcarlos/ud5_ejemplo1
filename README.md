# Ud5_Ejemplo1
_Ejemplo 1 de la Unidad 5._

Vamos a implementar una aplicación que acceda a la _SWAPI_ (Star Wars API) alojada en el servidor _https://swapi.dev/_, obtenga 
el nombre y género de los primeros 10 personajes almacenados y los liste en un _RecyclerView_.
Para ello vamos a hacer uso de la librería _Retrofit_ y obtendremos los datos utilizando el método HTTP _GET_.

Los pasos serán los siguientes:


## Paso 1: Modificar el fichero _build.gradle(Module:app)_
Añadiremos las siguientes dependencias para poder hacer uso de la librería _Retrofit_ y del convertidor de _JSON_ _GSON_
(Podemos ver la versión actual en _https://github.com/square/retrofit_):
``` html
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
```

Además, para su correcto funcionamiento, deberemos comprobar que las siguientes líneas están en el bloque _android_:
```html
android {
    ...
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    ...
}
```

## Paso 2: Dar permisos a la aplicación
Para poder realizar conexiones con servidores deberemos darle permisos a la aplicación. Para ello se debe añadir la siguiente línea en el fichero _AndrodManifest.xml_:

```html
<uses-permission android:name="android.permission.INTERNET"/>
```

## Paso 3: Creación de la interfaz _ApiStarWars_
Creamos una interfaz con los métodos HTTP a utilizar en nuestra aplicación. En este caso solo un método _GET_ sobre la ruta _people_ del servidor. 
Además declaramos un método para obtener la información de los personajes que devuelve un objeto _Call_ de tipo _Respuesta_.

```java
interface ApiStarWars {
    // Realizamos la consulta sobre la ruta "people" del servidor https://swapi.dev/api/ para
    // obtener los personajes.
    // Cada Call hace una petición HTTP asíncrona al servidor.
    @GET("people")
    fun obtenerPersonaje(): Call<Respuesta>
}
```

## Paso 4: Creación de la clase _Cliente_
Creamos una clase en la que construiremos el objeto _Retrofit_.

```java
class Cliente {
    companion object{
        const val URL:String = "https://swapi.dev/api/"
        var retrofit:Retrofit ?= null

        fun obtenerCliente():Retrofit? {
            if(retrofit == null){
                // Construimos el objeto Retrofit asociando la URL del servidor y el convertidor Gson
                // para formatear la respuesta JSON.
                retrofit = Retrofit.Builder()
                            .baseUrl(URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
            }

            return retrofit
        }
    }
}
```

## Paso 5: Creación de la clase _Personaje_
Creamos un _POJO_ con los datos que queremos obtener del personaje (nombre y género).

```java
data class Personaje (@SerializedName("name") var nombre: String,
                      @SerializedName("gender") var genero: String)
```

## Paso 6: Creación de la clase _Respuesta_
Creamos una clase que obtiene el array results del fichero _JSON_.

```java
data class Respuesta (@SerializedName("results") var resultados: ArrayList<Personaje>)
```

## Paso 7: Creación de los _layouts_
Creamos los _layouts_ de la aplicación con el _RecyclerView_.

### _activity_main.xml_
```html
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
### _elementos_lista.xml_
```html
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <TextView
        android:id="@+id/nombretextView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        tools:text="nombre"/>
    <TextView
        android:id="@+id/generotextView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        tools:text="genero"/>

</LinearLayout>
```

## Paso 8: Creación del adaptador _PersonajeAdapter_
Creamos el adaptador para trabajar con el _RecyclerView_.

```java
class PersonajeAdapter: RecyclerView.Adapter<PersonajeAdapter.MiViewHolder>() {

    private var lista: ArrayList<Personaje> = ArrayList()

    // Creamos nuestro propio ViewHolder
    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nombretextView: TextView
        val generotextView: TextView

        init {
            nombretextView = view.findViewById(R.id.nombretextView)
            generotextView = view.findViewById(R.id.generotextView)
        }
    }

    // Creamos nuevas views inflando el layout "elementos_lista"
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MiViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.elementos_lista, viewGroup, false)

        return MiViewHolder(view)
    }

    // Establecemos el nombre y la aparición para el personaje de esa posición
    override fun onBindViewHolder(viewHolder: MiViewHolder, position: Int) {
        viewHolder.nombretextView.text = lista[position].nombre
        viewHolder.generotextView.text = lista[position].genero
    }

    // Devolvemos el tamaño de la lista de personajes
    override fun getItemCount() = lista.size

    // Método para añadir la lista al Recyclerview
    fun anyadirALista(lista_: ArrayList<Personaje>){
        lista.clear()
        lista.addAll(lista_)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }
}
```

## Paso 9: Creación de la clase _MainActivity_
Por último, en la clase _MainActivity_ haremos uso de todas las clases creadas y podremos conectar con el servidor y listar los 
datos obtenidos.

```java
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private lateinit var recycler: RecyclerView
    private var retrofit: Retrofit ?= null
    private var personajeAdapter: PersonajeAdapter ?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Buscamos el RecyclerView e indicamos que su tamaño es fijo
        recycler = binding.recyclerview

        recycler.setHasFixedSize(true)

        // Añadimos la línea de separación de elementos de la lista
        // 0 para horizontal y 1 para vertical
        recycler.addItemDecoration(DividerItemDecoration(this, 1))

        // Asignamos un LinearLayout que contendrá cada elemento del RecyclerView
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        personajeAdapter = PersonajeAdapter()

        // Asignamos el adapter al RecyclerView
        recycler.adapter = personajeAdapter

        retrofit = Cliente.obtenerCliente()

        obtenerDatos()
    }

    // Método para acceder obtener los datos a través de la API y añadirlos a la lista.
    private fun obtenerDatos() {
        // Hacemos uso de la ApiStarWars creada para obtener los valores pedidos.
        val api: ApiStarWars? = retrofit?.create(ApiStarWars::class.java)

        // Obtenemos la respuesta.

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        api?.obtenerPersonaje()?.enqueue(object : Callback<Respuesta> {
            override fun onResponse(call: Call<Respuesta>, response: Response<Respuesta>) {
                if (response.isSuccessful) {
                    val respuesta = response.body()

                    if (respuesta != null) {
                        val listaPersonajes = respuesta.resultados
                        personajeAdapter?.anyadirALista(listaPersonajes)
                    }
                } else
                    Toast.makeText(
                        getApplicationContext(),
                        "Fallo en la respuesta",
                        Toast.LENGTH_SHORT
                    ).show()
            }

            override fun onFailure(call: Call<Respuesta>, t: Throwable) {
                Toast.makeText(getApplicationContext(), t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
```