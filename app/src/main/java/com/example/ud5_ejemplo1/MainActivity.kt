package com.example.ud5_ejemplo1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ud5_ejemplo1.conexion.ApiStarWars
import com.example.ud5_ejemplo1.conexion.Cliente
import com.example.ud5_ejemplo1.databinding.ActivityMainBinding
import com.example.ud5_ejemplo1.modelo.Respuesta
import retrofit2.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private lateinit var recycler: RecyclerView
    private var retrofit: Retrofit ?= null
    private var personajeAdapter: PersonajeAdapter ?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Buscamos el RecyclerView e indicamos que su tamaño es fijo
        recycler = binding.recyclerview

        recycler.setHasFixedSize(true)

        // Añadimos la línea de separación de elementos de la lista
        // 0 para horizontal y 1 para vertical
        recycler.addItemDecoration(DividerItemDecoration(this, 1))

        // Asignamos un LinearLayout que contendrá cada elemento del RecyclerView
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        personajeAdapter = PersonajeAdapter()

        // Asignamos el adapter al RecyclerView
        recycler.adapter = personajeAdapter

        retrofit = Cliente.obtenerCliente()

        obtenerDatos()
    }

    // Método para acceder obtener los datos a través de la API y añadirlos a la lista.
    private fun obtenerDatos() {
        // Hacemos uso de la ApiStarWars creada para obtener los valores pedidos.
        val api: ApiStarWars? = retrofit?.create(ApiStarWars::class.java)

        // Obtenemos la respuesta.

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        api?.obtenerPersonaje()?.enqueue(object : Callback<Respuesta> {
            override fun onResponse(call: Call<Respuesta>, response: Response<Respuesta>) {
                if (response.isSuccessful) {
                    val respuesta = response.body()

                    if (respuesta != null) {
                        val listaPersonajes = respuesta.resultados
                        personajeAdapter?.anyadirALista(listaPersonajes)
                    }
                } else
                    Toast.makeText(
                        getApplicationContext(),
                        "Fallo en la respuesta",
                        Toast.LENGTH_SHORT
                    ).show()
            }

            override fun onFailure(call: Call<Respuesta>, t: Throwable) {
                Toast.makeText(getApplicationContext(), t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}