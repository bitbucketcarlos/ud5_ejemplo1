package com.example.ud5_ejemplo1.modelo

import com.google.gson.annotations.SerializedName

data class Respuesta (@SerializedName("results") var resultados: ArrayList<Personaje>)