package com.example.ud5_ejemplo1.modelo

import com.google.gson.annotations.SerializedName

// Solo vamos a hacer uso de las propiedades de nombre y género del personaje.
// Usamos la anotación "SerializedName" para indicar que queremos obtener el valor de la
// clave indicada, así podemos usar otro nombre para el atributo del POJO. En otro caso
// el atributo se debe llamar igual a la clave.
data class Personaje (@SerializedName("name") var nombre: String,
                      @SerializedName("gender") var genero: String)