package com.example.ud5_ejemplo1.conexion

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Cliente {
    companion object{
        const val URL:String = "https://swapi.dev/api/"
        var retrofit:Retrofit ?= null

        fun obtenerCliente():Retrofit? {
            if(retrofit == null){
                // Construimos el objeto Retrofit asociando la URL del servidor y el convertidor Gson
                // para formatear la respuesta JSON.
                retrofit = Retrofit.Builder()
                            .baseUrl(URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
            }

            return retrofit
        }
    }
}