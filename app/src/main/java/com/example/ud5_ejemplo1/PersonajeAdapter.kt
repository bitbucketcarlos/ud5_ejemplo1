package com.example.ud5_ejemplo1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ud5_ejemplo1.modelo.Personaje

class PersonajeAdapter: RecyclerView.Adapter<PersonajeAdapter.MiViewHolder>() {

    private var lista: ArrayList<Personaje> = ArrayList()

    // Creamos nuestro propio ViewHolder
    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nombretextView: TextView
        val generotextView: TextView

        init {
            nombretextView = view.findViewById(R.id.nombretextView)
            generotextView = view.findViewById(R.id.generotextView)
        }
    }

    // Creamos nuevas views inflando el layout "elementos_lista"
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MiViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.elementos_lista, viewGroup, false)

        return MiViewHolder(view)
    }

    // Establecemos el nombre y la aparición para el personaje de esa posición
    override fun onBindViewHolder(viewHolder: MiViewHolder, position: Int) {
        viewHolder.nombretextView.text = lista[position].nombre
        viewHolder.generotextView.text = lista[position].genero
    }

    // Devolvemos el tamaño de la lista de personajes
    override fun getItemCount() = lista.size

    // Método para añadir la lista al Recyclerview
    fun anyadirALista(lista_: ArrayList<Personaje>){
        lista.clear()
        lista.addAll(lista_)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }
}